import React from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import ConditionalRendering from './Components/ConditionalRendering'
import ColorPicker from './Components/ColorPicker'


function App() {
  

  return (
    <>
      <ConditionalRendering />
      <ColorPicker />
    </>
  )
}

export default App
