import React, { useState } from "react";

function ConditionalRendering() {
    const [log, setLog] = useState(false)

    return(
        <>
            {log ? (
                <div>
                    <p>Welcome User</p>
                    <button onClick={() => setLog(!log)}>Log Out</button>
                </div>

                ) : (
                
                <div>
                    <button onClick={() => setLog(!log)}>Log in</button>
                </div>
            )}
        </>
    )
}

export default ConditionalRendering;