import React, { useState } from "react";

function ColorPicker() {
    
    const [color, setColor] = useState("green");
    
    return(
        <div>
            <p>Select Color : {color} </p>
            <button onClick={() => setColor("Red")}>Red</button>
            <button onClick={() => setColor("Blue")}>Blue</button>
            <button onClick={() => setColor("Green")}>Green</button>
        </div>
    )
}

export default ColorPicker;
