import React, { useState } from "react";
import './Calculator.scss';
import Button from "../Buttons/Button";

function CalculatorComponent() {
    const [value, setValue] = useState('');

    const printValue = (val) => {
        setValue((prevValue) => prevValue + val);
    };

    const deleteValue = () => {
        setValue((prevValue) => prevValue.slice(0, -1));
    };

    const result = () => {  
        setValue(eval(value));
  
    };

    const clearValue = () => {
        setValue('');
    };

    return (
        <div className="calculator">
            <div className="value-input">
                <input type="text" value={value}/>
            </div>

            <div className="buttons-bar">
                <div className="d-flex">
                    <Button symbol={'AC'} clickBtn={clearValue} classbtn={'black-color'} color={'#4535C1'} />
                    <Button symbol={'DE'} clickBtn={deleteValue} classbtn={'black-color'} color={'#4535C1'} />
                    <Button symbol={'%'} clickBtn={printValue} classbtn={'black-color'} color={'#4535C1'} />
                    <Button symbol={'/'} clickBtn={printValue} color={'rgb(57, 77, 109)'} classbtn={'equal'}/>
                </div>

                <div className="d-flex">
                    <Button symbol={'7'} clickBtn={printValue} color={'#346699'} />
                    <Button symbol={'8'} clickBtn={printValue} color={'#346699'} />
                    <Button symbol={'9'} clickBtn={printValue} color={'#346699'} />
                    <Button symbol={'*'} clickBtn={printValue} color={'rgb(57, 77, 109)'} classbtn={'equal'}/>
                </div>

                <div className="d-flex">
                    <Button symbol={'4'} clickBtn={printValue} color={'#346699'} />
                    <Button symbol={'5'} clickBtn={printValue} color={'#346699'} />
                    <Button symbol={'6'} clickBtn={printValue} color={'#346699'} />
                    <Button symbol={'-'} clickBtn={printValue} color={'rgb(57, 77, 109)'} classbtn={'equal'}/>
                </div>

                <div className="d-flex">
                    <Button symbol={'1'} clickBtn={printValue} color={'#346699'} />
                    <Button symbol={'2'} clickBtn={printValue} color={'#346699'} />
                    <Button symbol={'3'} clickBtn={printValue} color={'#346699'} />
                    <Button symbol={'+'} clickBtn={printValue} color={'rgb(57, 77, 109)'} classbtn={'equal'} />
                </div>

                <div className="d-flex">
                    <Button symbol={'0'} clickBtn={printValue} classbtn={'zero'} color={'#346699'} />
                    <Button symbol={'.'} clickBtn={printValue} color={'#346699'} />
                    <Button symbol={'='} classbtn={'equal'} clickBtn={result} color={'#4535C1'} />
                </div>
            </div>
        </div>
    );
}

export default CalculatorComponent;
