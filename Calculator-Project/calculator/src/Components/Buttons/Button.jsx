import React from "react";
import "./Button.scss"

function Button({symbol, color, classbtn, clickBtn}){
    return( 
        <button onClick={() => clickBtn(symbol)} style={{backgroundColor: color}} className={classbtn}>{symbol}</button>
    )
}

export default Button;